package com.progressoft.induction.atm;

import com.progressoft.induction.atm.exceptions.AccountNotFoundException;
import com.progressoft.induction.atm.exceptions.InsufficientFundsException;
import com.progressoft.induction.atm.exceptions.NotEnoughMoneyInATMException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class BankingSystemImpl implements BankingSystem, ATM {

   private final Map<String, BigDecimal> userAccount = new HashMap<>();
   //as per table data provided
   private       BigDecimal              moneyInBank = new BigDecimal(2400);


   public BankingSystemImpl() {
      userAccount.put("123456789", BigDecimal.valueOf(1000.0));
      userAccount.put("111111111", BigDecimal.valueOf(1000.0));
      userAccount.put("222222222", BigDecimal.valueOf(1000.0));
      userAccount.put("333333333", BigDecimal.valueOf(1000.0));
      userAccount.put("444444444", BigDecimal.valueOf(1000.0));
   }

   @Override
   public BigDecimal getAccountBalance(String accountNumber) {
      return userAccount.get(accountNumber);
   }

   @Override
   public void debitAccount(String accountNumber, BigDecimal amount) {
      BigDecimal availableBalance = getAccountBalance(accountNumber);
      userAccount.put(accountNumber, availableBalance.subtract(amount));
   }

   @Override
   public List<Banknote> withdraw(String accountNumber, BigDecimal amount) {
      if (checkIfUserExists(accountNumber)) {
         if (checkUserBalance(accountNumber, amount)) {
            if (checkIfMoneyInBank(amount)) {
               debitAccount(accountNumber, amount);
               deductMoneyInBank(amount);
            } else {
               throw new NotEnoughMoneyInATMException();
            }

         } else {
            throw new InsufficientFundsException();
         }

      } else {
         throw new AccountNotFoundException();
      }
      return getBankNotes(amount.intValue());
   }

   private boolean checkIfMoneyInBank(BigDecimal amount) {
      return moneyInBank.compareTo(amount) > 0;
   }

   private boolean checkUserBalance(String accountNumber, BigDecimal amount) {
      return getAccountBalance(accountNumber).compareTo(amount) > 0 ||
          getAccountBalance(accountNumber).compareTo(amount) == 0;
   }

   private void deductMoneyInBank(BigDecimal amount) {
      moneyInBank = moneyInBank.subtract(amount);
   }

   private boolean checkIfUserExists(String accountNumber){
      return userAccount.containsKey(accountNumber);
   }

   private List<Banknote> getBankNotes(int amount) {
      int[] notes = {50, 20, 10, 5};
      int[] notesFrequency = {0, 0, 0, 0};
      Random random = new Random();

      while (amount > 0) {
         int current = random.nextInt(4);
         if (amount >= notes[current]) {
            amount -= notes[current];
            notesFrequency[current] += 1;
         }
      }
      List<Banknote> list = new ArrayList<>();
      for (int i = 0; i < 4; i++) {
         for (int j = 0; j < notesFrequency[i]; j++) {
            if (i == 0) {
               list.add(Banknote.FIFTY_JOD);
            }
            if (i == 1) {
               list.add(Banknote.TWENTY_JOD);
            }
            if (i == 2) {
               list.add(Banknote.TEN_JOD);
            }
            if (i == 3) {
               list.add(Banknote.FIVE_JOD);
            }
         }
      }
      return list;
   }

}
